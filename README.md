# fermtroller #

Uses a Raspberry Pi and an LSM9DS0's temperature sensor to report temperature readings via a web interface. Also attempts to maintain temperature within a given range via heating. This turns a GPIO pin on and off to enable/disable heating, but you would need an actual heater for this to achieve anything.