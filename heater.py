import RPi.GPIO as GPIO
from smbus import SMBus
import time

GPIO.setmode(GPIO.BCM)


class Pin(object):
  PIN = 21
  MODE = GPIO.BCM

  def __init__(self, pin):
    self.pin = pin
    GPIO.setup(self.pin, GPIO.OUT)

  def on(self):
    GPIO.output(self.pin, 1)

  def off(self):
    GPIO.output(self.pin, 0)


class TempSensor(object):
  ADDRESS = 0x1D  # 3B >> 1 = 7bit default
  REGISTER_TEMP_OUT_L  = 0x05
  REGISTER_TEMP_OUT_H  = 0x06
  REGISTER_CTRL_REG5   = 0x24
  BUS = 1


  def __init__(self):
    self.i2c = SMBus(TempSensor.BUS)

    # enable temperature sensor
    self.i2c.write_byte_data(TempSensor.ADDRESS,
        TempSensor.REGISTER_CTRL_REG5, 0b10011000)

  def read(self):
    low = self.i2c.read_byte_data(TempSensor.ADDRESS,
        TempSensor.REGISTER_TEMP_OUT_L)
    high = self.i2c.read_byte_data(TempSensor.ADDRESS,
        TempSensor.REGISTER_TEMP_OUT_H)
    return (low | (high << 8)) / 2

  def close(self):
    self.i2c.close()


class Heater(object):
  def __init__(self, sensor, heater):
    self.sensor = sensor
    self.heater = heater
    self.low = 18
    self.high = 22

    self.current = 0

  def setLow(self, value):
    self.low = min(max(0, value), self.high - 1)

  def setHigh(self, value):
    self.high = max(self.low + 1, min(40, value))

  def tick(self):
    self.current = self.sensor.read()
    position = (self.current - self.low) / float(self.high - self.low)
    print self.current, position, self.low, self.high
    if position <= 0.5:
      self.heater.on()
    else:
      self.heater.off()

  def close():
      self.sensor.close()

  def run(self):
    while True:
      self.tick()
      time.sleep(3)


def cleanup():
  GPIO.cleanup()
