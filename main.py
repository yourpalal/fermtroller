#!/usr/bin/env python

import json, threading

import heater

heat = None


from flask import Flask, request, send_from_directory
app = Flask(__name__)


@app.route("/status.json")
def status():
  return json.dumps({
      "low": heat.low,
      "high": heat.high,
      "current": heat.current
  })

@app.route("/")
def index(): 
  return send_from_directory("static", "index.html", as_attachment=False)


@app.route("/high", methods=['PUT'])
def setHigh():
  heat.setHigh(int(request.data))
  return ''

@app.route("/low", methods=['PUT'])
def setLow():
  heat.setLow(int(request.data))
  return ''

if __name__ == '__main__':
  # we only want to make heat once, even if we are restarted by debug mode
  if heat is None:
    heat = heater.Heater(heater.TempSensor(), heater.Pin(21))
    threading.Thread(target=heat.run).start()

  app.run(host='0.0.0.0')
