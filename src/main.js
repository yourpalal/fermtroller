var React = require('react');
var ReactDOM = require('react-dom');

var Temperature = React.createClass({
  getInitialState: function() {
    return {
      low: 0,
      high: 50,
      current: -100
    };
  },

  componentDidMount() {
    this.getUpdatedInfo();
    window.setInterval(this.getUpdatedInfo, 3000);
  },

  getUpdatedInfo() {
    var req = new XMLHttpRequest();
    var self = this;
    req.onreadystatechange = function() {
      if (req.readyState != XMLHttpRequest.DONE) {
        return;
      }
      if (req.status == 200) {
        self.setState(JSON.parse(req.responseText));
      }

    };
    req.open("GET", "status.json", true);
    req.send();
  },

  setLow: function(event) {
    this.setState({low: event.target.value});
    this.put("/low", event.target.value);
  },

  setHigh: function(event) {
    this.setState({high: event.target.value});
    this.put("/high", event.target.value);
  },

  put: function(route, data) {
    var req = new XMLHttpRequest();

    req.open("PUT", route, true);
    req.setRequestHeader("Content-Type", "text/plain");
    req.send(data.toString());
    this.getUpdatedInfo();
  },

  render: function() {
    return (
      <div className="row">
        <h1 className="col-md-3">We're cooking at {this.state.current}</h1>
        <div className="col-md-9">
           and keeping it between
          <input type="number" onChange={this.setLow} value={this.state.low} />
          and
          <input type="number" onChange={this.setHigh} value={this.state.high} />
        </div>
      </div>
    );
  }
});


window.onload = function() {
  ReactDOM.render(
      <Temperature />,
      document.getElementsByTagName('div')[0]
  );
};
